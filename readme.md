# Dads Backup

## Detail

This is a small script that automates the creation, testing and logging of backups in a zip archive.

The script asks where to take data from and where to save it.

It then creates a dated folder and creates a zip file containing the chosen files within that folder along with a log.txt file that contains and index of the included files.

The script then tests the zip fil integrity and appends the outcome to the log file

---

Setup involves using chocolatey to install 7zip CLI and all dependencies for simplicity

This script could be scheduled in task scheduler ot run manually

## Setup

* Share a folder on target device
* Map that folder as a network drive [https://support.microsoft.com/en-gb/windows/map-a-network-drive-in-windows-10-29ce55d1-34e3-a7e2-4801-131475f9557d]
* Install chocolatey. Run powershell as admin and paste in this

        Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))

* Close and reopen powershell admin and paste in this

        choco install 7zip -y

* Copy the backup.bat so somewhere on your computer

## Usage

* Turn on the old laptop and wait until it is logged in

* Run backup.bat on the new laptop and follow the instructions
