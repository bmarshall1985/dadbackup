@echo off

echo Dad Backup Utility

:: Collects target and source locations from input

echo Where do you want to backup to?
echo Enter a file path eg C:\Windows
echo. 
SET /P _target= Please enter an input:

cls 

echo Where to you want to backup from?
echo Enter a file path eg C:\Windows
echo. 
SET /P _source= Please enter an input:

cls

:: Preps the current date and time removing / delimters for use in folder and file naming

set filedatetime=%date%
for /F "tokens=1-3 delims=/ " %%a in ("%filedatetime%") do (
   set filedatetime=%%c%%b%%a
)

:: Start of backup, using locations and dates specified above

echo Creating Backup... This can take a while.

:: Creates dated folder in the target location

mkdir %target%/%filedatetime%

:: Zips Files and writes output to log

7z a %_target%/%filedatetime%/Backup%filedatetime% %_source% -r -bb3 > %_target%/%filedatetime%/log%filedatetime%.txt

:: tests and appends output to log

echo Backup completed. Testing...

7z t %_target%/%filedatetime%/Backup%filedatetime%.7z >> %_target%/%filedatetime%/log%filedatetime%.txt
 
echo Backup completed. See the log file for details
PAUSE